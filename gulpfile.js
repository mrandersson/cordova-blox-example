var gulp = require('gulp');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var watchify = require('watchify');
var browserify = require('browserify');
var gulpif = require('gulp-if');
var colors = require('colors');

/**
 * Files to be "browserified" to separate files.
 * Separate js file per view so no unneccessary js is loaded for each view.
 */
var bundles = [
  {
    input      : ['./src/javascripts/bundles/start.js'],
    output     : 'start.js',
    destination: './www/javascripts/bundles/'
  }
];

/**
 * Helper function to create a bundle for every file in bundles array,
 * @param {boolean} production Should files be built for production?
 */
function createBundle(options, production) {
  var bundler = watchify(browserify(options.input, {
    extensions: ['.js', '.jsx']
  }));

  bundler.transform('brfs');
  bundler.transform('reactify');

  var rebundle = function() {
    var startTime = new Date().getTime();
    bundler.bundle()
      // log errors if they happen
      .on('error', gutil.log.bind(gutil, 'Browserify Error'))
      .pipe(source(options.output))
      // optional, remove if you dont want sourcemaps
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
        .pipe(sourcemaps.write('./')) // writes .map file
      .on('end', function() {
        var time = (new Date().getTime() - startTime) / 1000;
        console.log("Finished browserify on file " + options.output.cyan + " in " + (time + 's').magenta);
      })
      .pipe(gulp.dest(options.destination));
  }
  // Rebuild file on update
  bundler.on('update', rebundle);

  rebundle();
}

/**
 * Build assets for development, serve html and livereload
 */
gulp.task('serve', function() {
  bundles.forEach(function(bundle) {
    createBundle(bundle, false);
  });
});
